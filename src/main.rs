use std::collections::HashMap;
use std::net::IpAddr;
use std::str::FromStr;
use tracing_subscriber::fmt::format::FmtSpan;
use warp::{http::Response, Filter};

fn load_settings() -> HashMap<String, String> {
    let mut settings = config::Config::default();
    settings
        // Add in `./rginx.toml`
        .merge(config::File::with_name("rginx"))
        .unwrap()
        // Add in settings from the environment (with a prefix of APP)
        // Eg.. `RGINX_DEBUG=1 ./target/app` would set the `debug` key
        .merge(config::Environment::with_prefix("RGINX"))
        .unwrap();

    settings.try_into::<HashMap<String, String>>().unwrap()
}

#[tokio::main]
async fn main() {
    // GET /hello/warp => 200 OK with body "Hello, warp!"
    // let hello = warp::path!("hello" / String).map(|name| format!("Hello, {}!", name));
    //
    // let _sum_route = warp::path!("sum" / u32 / u32).map(|a, b| {
    //     format!("{} + {} = {}", a, b, a + b);
    // });
    let s = load_settings();

    let filter = std::env::var("RUST_LOG").unwrap_or_else(|_| "tracing=info,warp=debug".to_owned());
    tracing_subscriber::fmt()
        // Use the filter we built above to determine which traces to record.
        .with_env_filter(filter)
        // Record an event when each span closes. This can be used to time our
        // routes' durations!
        .with_span_events(FmtSpan::CLOSE)
        .init();

    let routes = warp::any()
        .map(|| {
            Response::builder()
                .header("Server", "rginx")
                .header("X-Frame-Options", "DENY")
                .header(
                    "Cache-Control",
                    "max-age=2592000, stale-while-revalidate=86400",
                )
                .header("Content-Type", "text/plain; charset=utf-8")
                .header(
                    "Content-Security-Policy",
                    "default-src 'self'; object-src 'none'",
                )
                .body("Timatooth! 🦷")
        })
        .with(warp::trace::request());

    let addr = IpAddr::from_str("::0").unwrap();

    let cert_path = match s.get("cert_path") {
        Some(path) => path,
        None => panic!("need a cert_path config"),
    };

    let key_path = match s.get("key_path") {
        Some(path) => path,
        None => panic!("need a key_path config"),
    };

    let https_port = match s.get("https_port") {
        Some(port) => port.parse::<u16>().unwrap(),
        None => 4430,
    };

    // ssl only
    warp::serve(routes)
        .tls()
        .cert_path(cert_path)
        .key_path(key_path)
        .run((addr, https_port))
        .await;
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
